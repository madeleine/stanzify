import createDebug from 'debug';
import getStdin from 'get-stdin';
import fs from 'node:fs/promises';
import { stanzify } from './index.js';

export const debug = createDebug(stanzify.name);

class AbstractError extends Error {
  static strings = {
    1: 'Fatal error',
    2: 'Parse error',
    3: 'File I/O error',
  };

  constructor(message, exitCode) {
    if (new.target === AbstractError) {
      throw new TypeError(`can't instantiate an abstract class`);
    }
    super(message || AbstractError.strings[exitCode]);
    this.exitCode = exitCode;
  }

  static subclass(exitCode) {
    return class extends AbstractError {
      constructor(message) {
        super(message, exitCode);
      }
    }
  }
}

export const FatalError = AbstractError.subclass(1);
export const ParseError = AbstractError.subclass(2);
export const FileIOError = AbstractError.subclass(3);

export async function readInput(cli) {
  const pipedInput = await getStdin();

  let fileInput;
  const [ file ] = cli.input;
  if (file) {
    debug('%o', { file });
    try {
      fileInput = await fs.readFile(file, 'utf8');
    } catch (err) {
      throw new FileIOError(err.message);
    }
  }

  let contents;
  if (fileInput && pipedInput) {
    throw new ParseError('expected either file or stdin stream only');
  } else if (fileInput && !pipedInput) {
    contents = fileInput;
  } else if (!fileInput && pipedInput) {
    contents = pipedInput;
  } else {
    throw new ParseError('input as file or stdin stream is required');
  }

  debug('%o', { contents });
  return contents;
}
