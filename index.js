import pull from 'lodash.pull';
import sample from 'lodash.sample';
import { detectNewline } from 'detect-newline';
import { debug, FatalError } from './utils.js';

function pickRandomLine(lines) {
  const line = sample(lines);
  pull(lines, line);
  return line?.trim();
}

export const defaults = {
  size: 2,
};

export function stanzify(str, options = defaults) {
  const newline = detectNewline(str);
  if (!newline) {
    throw new FatalError('expected more than one line');
  }

  const lines = str.split(newline).filter(Boolean);
  let output = '';

  while (lines.length > 0) {
    if (options.size > 0) {
      for (let i = 0; i < options.size; i++) {
        const line = pickRandomLine(lines);
        if (line?.length > 0) {
          output += line;
        }
        output += newline;
        debug('%o', { options, i, line, lines, output });
      }
    } else {
      const line = pickRandomLine(lines);
      if (line?.length > 0) {
        output += line;
      }
      debug('%o', { options, line, lines, output });
    }
    output += newline;
  }

  return output.trim();
}
