import test from 'ava';
import { detectNewline } from 'detect-newline';
import fs from 'node:fs/promises';
import path from 'node:path';
import url from 'node:url';
import { stanzify } from './index.js';
import { debug } from './utils.js';

test.before(async t => {
  const dir = path.dirname(url.fileURLToPath(import.meta.url));
  const file = path.resolve(dir, 'To Make a Dadaist Poem.txt');
  const input = await fs.readFile(file, 'utf8');
  t.context.input = input;

  const output = stanzify(input);
  t.context.output = output;
});

test('all lines from the input are included in the output', t => {
  const { input, output } = t.context;
  const newline = detectNewline(input);
  t.true(output.split(newline).every(line => input.includes(line)));
});

test('output is unique each time', async t => {
  const prev = t.context.output;
  const next = await stanzify(t.context.input);
  t.not(next, prev);
});
