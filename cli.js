#!/usr/bin/env node
import meow from 'meow';
import chalk from 'chalk';
import isMain from 'is-main';
import { stanzify, defaults } from './index.js';
import { debug, readInput } from './utils.js';

if (isMain(import.meta)) {
  process.title = stanzify.name;

  const cli = meow(
    `Usage:
  $ ${stanzify.name} [file] [options]  # or pipe text via stdin

Options:
  --size        Lines per stanza; use 0 for one stanza (${defaults.size})
  --help        Print this message and exit
  --version     Print the version string and exit`,
    {
      importMeta: import.meta,
      description: 'Shuffle and arrange a list of lines into stanzas',
      flags: {
        size: {
          alias: 's',
          type: 'number',
          default: defaults.size,
        },
      },
    },
  );

  const { flags, input } = cli;
  debug('%o', { flags, input, 'process.argv': process.argv });

  try {
    const text = await readInput(cli);
    console.log(stanzify(text, flags));
  } catch (err) {
    debug('%s', chalk.red.bold(err.stack));
    console.error(`${stanzify.name}: ${err.message}`);
    console.error(`try '${stanzify.name} --help' for more information.`);
    process.exitCode = err.exitCode;
  }
}
