# stanzify

Shuffle and arrange a list of lines into stanzas 📝

## Installation

This package is not currently published on an npm registry, so you have to clone the git repo:

``` sh
$ git clone git@codeberg.org:madeleine/stanzify.git && cd $_
```

## Usage

You can use stanzify as either a command line program or as an ECMAScript module (ESM).

### CLI

To use stanzify as a command line program, install it globally and then run it like this:

``` sh
$ npm install --global /path/to/stanzify
$ stanzify To\ Make\ a\ Dadaist\ Poem.txt --size 3 > out.txt
$ echo hello\nworld\nfoo\nbar | stanzify > out2.txt
```

### ESM

To use stanzify as an ECMAScript module, first install it as a project dependency:

``` sh
npm install -E /path/to/stanzify
```

Or use `npm link`, which is left as an exercise for the reader. 😼

Then import it and use it like this:

``` jsx
import { stanzify } from 'stanzify';
import fs from 'node:fs/promises';

const text = await fs.readFile('./To Make a Dadaist Poem.txt', 'utf8');
console.log(stanzify(text, { size: 3 }));
```

See [Modules: ECMAScript modules (Node.js documentation)](https://nodejs.org/api/esm.html). Make sure you're using either the `.mjs` file extension, the `"type": "package"` field in `package.json`, or the `--input-type=module` CLI flag.

### API

| option   | type     | description                            | default value |
|----------|----------|----------------------------------------|---------------|
| `size`   | `number` | Lines per stanza; use 0 for one stanza | `2`           |

## Development

### Running tests

Run the test suite with `npm run test`.

To run the test suite in [watch mode](https://github.com/avajs/ava/blob/main/docs/recipes/watch-mode.md), use `npm run test:watch`.

### Debugging

Debug ouput can be enabled by setting the DEBUG environment variable:

```sh
$ DEBUG=stanzify stanzify -h
$ DEBUG=stanzify npm test
```

See [debug-js/debug](https://github.com/debug-js/debug/#usage).

## License

[GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)

## Corpora

- [Gutenberg, dammit](https://github.com/aparrish/gutenberg-dammit)
- [The Lovecraft Corpus](https://github.com/vilmibm/lovecraftcorpus)
- [Plain text example files](https://github.com/aparrish/plaintext-example-files)

## See also

- [prosaic](https://github.com/vilmibm/prosaic)
- [Reading and Writing Electronic Text](https://github.com/aparrish/rwet)
- [Tracery](https://github.com/ryanfarber/tracery)
